defmodule ExBanking.Core do
  alias ExBanking.{Balance, User}

  def send(from_username, to_username, amount, currency) do
    with user when is_map(user) <- User.find(from_username),
         x_user when is_map(x_user) <- User.find(to_username),
         {:ok, from_balance} <- Balance.withdraw(from_username, amount, currency),
         {:ok, to_balance} <- Balance.deposit(to_username, amount, currency) do
      {:ok, from_balance, to_balance}
    else
      error ->
        error
    end
  end
end
