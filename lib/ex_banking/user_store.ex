defmodule ExBanking.UserStore do
  use Agent

  def start_link(_args) do
    Agent.start_link(fn -> [] end, name: __MODULE__)
  end

  def create(username) do
    case find(username) do
      user when is_map(user) ->
        {:error, :user_already_exists}

      nil ->
        balance = %{"USD" => 0.00}
        user = %{name: username, balances: balance}

        update(user)
    end
  end

  def find(username) do
    Agent.get(__MODULE__, fn users ->
      Enum.find(users, fn u ->
        u.name == username
      end)
    end)
  end

  def update(user, balances) do
    Agent.update(__MODULE__, fn users ->
      Enum.map(users, fn u ->
        if u == user do
          %{u | balances: balances}
        else
          u
        end
      end)
    end)
  end

  def update(user) do
    Agent.update(__MODULE__, fn users ->
      users ++ [user]
    end)
  end
end
