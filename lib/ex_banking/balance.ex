defmodule ExBanking.Balance do
  alias ExBanking.UserStore

  defguard is_positive_number(number) when is_number(number) and number > 0.00

  def deposit(username, amount, currency) when is_positive_number(amount) do
    with user when is_map(user) <- UserStore.find(username) do
      balances = update_balances(user, currency, amount)
      UserStore.update(user, balances)

      {:ok, Map.get(balances, currency)}
    else
      error ->
        error
    end
  end

  def deposit(_, _, _), do: {:error, :wrong_arguments}

  def withdraw(username, amount, currency) when is_number(amount) do
    with user when is_map(user) <- UserStore.find(username),
         {:ok, currency_amount} <- find_currency(user, currency),
         :ok <- check_positive_balance(currency_amount, amount) do
      balances = update_balances(user, currency, -amount)
      UserStore.update(user, balances)

      {:ok, Map.get(balances, currency)}
    else
      error ->
        error
    end
  end

  def withdraw(_, _, _), do: {:error, :wrong_arguments}

  def get_balance(username, currency) do
    with user when is_map(user) <- UserStore.find(username),
         {:ok, amount} <- find_currency(user, currency) do
      {:ok, amount}
    else
      error ->
        error
    end
  end

  defp update_balances(user, currency, amount) do
    case find_currency(user, currency) do
      {:ok, current_amount} ->
        Map.put(user.balances, currency, current_amount + amount)

      {:error, _} ->
        Map.put(user.balances, currency, amount)
    end
  end

  defp find_currency(user, currency) do
    case Map.get(user.balances, currency) do
      amount when is_number(amount) ->
        {:ok, amount}

      nil ->
        {:error, :not_enough_money}
    end
  end

  defp check_positive_balance(currency, amount) do
    case currency - amount >= 0 do
      true ->
        :ok

      false ->
        {:error, :not_enough_money}
    end
  end
end
