defmodule ExBanking.User do
  @type user :: %{name: String.t(), balances: list(balance)}
  @type balance :: %{amount: float(), currency: String.t()}

  alias ExBanking.UserStore

  def create(username), do: UserStore.create(username)

  def find(username) do
    case UserStore.find(username) do
      user when is_map(user) ->
        user

      nil ->
        {:error, :user_does_not_exist}
    end
  end
end
