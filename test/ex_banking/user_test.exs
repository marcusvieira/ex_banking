defmodule ExBanking.UserTest do
  use ExUnit.Case

  alias ExBanking.User

  describe "create/1" do
    test "creates a user" do
      assert :ok = User.create("test")
    end
  end

  describe "find/1" do
    test "when user exists" do
      User.create("johndoe")

      user = User.find("johndoe")
      assert %{name: "johndoe"} = user
    end

    test "when user does not exists" do
      user = User.find("nothere")
      assert {:error, :user_does_not_exist} = user
    end
  end
end
