- Add back-pressure w/ rate-limiting (GenStage)
- `send/4` does not return the correct errors
- Add specs (!)
- Add type notation to methods 
- Rename + reorganize module names
- Documenting each module with `@moduledoc`

